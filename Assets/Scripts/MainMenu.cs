﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public InputField playerName;
    public void StartGame()
    {
        if (playerName.text != "")
        {
            GameData.SetName(playerName.text);
            SceneManager.LoadScene(1);
        }
        else
        {
            GameData.SetName("Player1");
            SceneManager.LoadScene(1);
        }
    }

    public void ShowKeyboard()
    {
        System.Diagnostics.Process.Start("osk.exe");
    }
}
