﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public InputHandler _input;
    public List<Block> allBlocks;
    //public float timerDuration;
    public GameObject scoreWindow;
    public Text myScore;
    public Text highScore;
    public Text playerName;

    private float _timer = 0f;
    private bool isTimerActive = false;

    void Start()
    {
        GameData.SetGameType(GameData.GameType.PLUMBER);
        EnableTimer();
    }

    private IEnumerator FillAll()
    {
        //foreach (var block in allBlocks)
        //{
        //    yield return StartCoroutine(block.Fill());
        //}
        for (int i = allBlocks.Count-1; i >= 0; i--)
        {
            yield return StartCoroutine(allBlocks[i].Fill());
        }
        Debug.Log("filled all");
    }

    private void ResetAllBlocks()
    {
        //reset all blocks
        foreach (var block in allBlocks)
        {
            block.Reset();
        }
        //hide score
        //enable input

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetAllBlocks();
        }

        if (isTimerActive)
        {
            _timer += Time.deltaTime;
            //TimerImage.fillAmount = _timer / timerDuration;
        }

    }

    public IEnumerator CheckBoard()
    {
        _input.acceptingClicks = false;
        bool result = true;
        foreach (var block in allBlocks)
        {
            if (!block.IsCorrectOrientation())
            {
                result = false;
            }
        }
        if (result)
        {
            StopTimer();
            GameData.AddScore((int)_timer);
            myScore.text = GameData.Score.ToString()+"s";
            highScore.text = GameData.GetHighScore().ToString()+"s";
            playerName.text = GameData.GetName();
            yield return StartCoroutine(FillAll());
            //calculate score then show etc.
            ShowScore();
        }
        else
        {
            _input.acceptingClicks = true;
        }


    }

    public void EnableTimer()
    {
        _timer = 0f;
        isTimerActive = true;
    }

    public void StopTimer()
    {
        isTimerActive = false;
    }

    public void ShowScore()
    {
        scoreWindow.SetActive(true);
    }


    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

   
}
