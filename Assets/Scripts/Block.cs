﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Block : MonoBehaviour, IPointerClickHandler
{
    public enum Shapes
    {
        Straight,
        Corner
    }
    public InputHandler _input;
    public GameManager _gamemanager;
    public Shapes myShape;
    public List<float> orientations;
    public float rightOrientation;
    int currentOrientationIndex;

    private int startingOrientationIndex;

    RectTransform myRectTransform;
    public Image liquidImage;

    // Use this for initialization
    void Start()
    {
        myRectTransform = GetComponent<RectTransform>();
        orientations = new List<float>();
        switch (myShape)
        {
            case Shapes.Straight:
                orientations.Add(0);
                orientations.Add(90);
                for (int i = 0; i < orientations.Count; i++)
                {
                    if (orientations[i] == myRectTransform.eulerAngles.z)
                    {
                        currentOrientationIndex = i;
                    }
                }
                break;
            case Shapes.Corner:
                orientations.Add(0);
                orientations.Add(90);
                orientations.Add(180);
                orientations.Add(270);
                for (int i = 0; i < orientations.Count; i++)
                {
                    if (orientations[i] == myRectTransform.eulerAngles.z)
                    {
                        currentOrientationIndex = i;
                    }
                }
                break;
            default:
                break;
        }
       
       

        //orientations.Add(myRectTransform.eulerAngles.z);
        //currentOrientationIndex = orientations.Count - 1;

        startingOrientationIndex = currentOrientationIndex;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_input.acceptingClicks)
        {
            HandleClick();
        }
    }

    void HandleClick()
    {
        //increment or reset index
        if (currentOrientationIndex == orientations.Count - 1)
        {
            currentOrientationIndex = 0;
        }
        else
        {
            currentOrientationIndex++;
        }

        //apply orientation
        ApplyOrientation();
        StartCoroutine(_gamemanager.CheckBoard());
        //StartCoroutine(Fill());
    }

    void ApplyOrientation()
    {
        //get orientation from list
        float tempValue = orientations[currentOrientationIndex];

        //get current orientation
        Vector3 currentRotation = myRectTransform.eulerAngles;

        //apply rotation
        myRectTransform.eulerAngles = new Vector3(currentRotation.x, currentRotation.y, tempValue);
    }

    public IEnumerator Fill()
    {
        for (float a = liquidImage.fillAmount; a <= 1.1; a += 0.1f)
        {
            liquidImage.fillAmount = a;
            yield return null;
        }
    }

    public void Reset()
    {
        currentOrientationIndex = startingOrientationIndex;
        ApplyOrientation();
    }

    public bool IsCorrectOrientation()
    {
        if (orientations[currentOrientationIndex] == rightOrientation)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
